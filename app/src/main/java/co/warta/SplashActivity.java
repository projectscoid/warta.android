package co.warta;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends Activity {
	public static SplashActivity instance;

	private Timer tmr_splash;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
		setContentView(R.layout.activity_splash);
	}

	@Override
	protected void onResume() {
		AdsController.init();
		super.onResume();
		setInitial();
	}

	@Override
	protected void onDestroy() {
		instance = null;
		super.onDestroy();
	}

	private void setInitial() {
		tmr_splash = new Timer();
		tmr_splash.schedule(new SplashTimer(), 2000, 2000);
	}

	private void openMainActivity() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				final Intent intent = new Intent(App.getContext(), MainActivity.class);
				startActivity(intent);
				finish();
			}
		});
	}

	private class SplashTimer extends TimerTask {
		@Override
		public void run() {
			tmr_splash.cancel();
			tmr_splash = null;
			openMainActivity();
		}
	}
}