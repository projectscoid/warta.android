package co.warta;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

public class GlobalController {
	public static String getDeviceID() {
		try {
			final TelephonyManager telephony_manager = (TelephonyManager) App.getContext().getSystemService(Context.TELEPHONY_SERVICE);
			if (telephony_manager.getDeviceId() != null) {
				return telephony_manager.getDeviceId();
			} else {
				return Settings.Secure.getString(App.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
			}
		} catch (SecurityException ex) {
		} catch (ExceptionInInitializerError ex) {
		}
		return "";
	}

	public static String getString(final int res_id) {
		return App.getContext().getString(res_id);
	}

	public static boolean isValidOS() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			return true;
		} else {
			return false;
		}
	}

	public static void executeAsync(final AsyncTask<Void, Void, Void> async_task) {
		if (isValidOS()) {
			executeValidAsync(async_task);
		} else {
			executeNonValidAsync(async_task);
		}
	}

	private static void executeValidAsync(final AsyncTask<Void, Void, Void> async_task) {
		async_task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	private static void executeNonValidAsync(final AsyncTask<Void, Void, Void> async_task) {
		async_task.execute();
	}
}