package co.warta;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class AdsController {
	private static InterstitialAd ad_interstitial;
	private static AdRequest ad_request;
	private static boolean test = false;

	public static void init() {
		if (test) {
			ad_request = new AdRequest.Builder().addTestDevice(GlobalController.getDeviceID()).build();
		} else {
			ad_request = new AdRequest.Builder().build();
		}
		ad_interstitial = new InterstitialAd(App.getContext());
		ad_interstitial.setAdUnitId(GlobalController.getString(R.string.interstitial_ad_id));
		ad_interstitial.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				AdsController.show();
			}
		});
		ad_interstitial.loadAd(ad_request);
	}

	public static void show() {
		if (ad_interstitial != null && ad_interstitial.isLoaded()) {
			ad_interstitial.show();
		}
	}
}